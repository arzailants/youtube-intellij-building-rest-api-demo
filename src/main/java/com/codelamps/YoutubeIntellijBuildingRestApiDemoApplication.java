package com.codelamps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YoutubeIntellijBuildingRestApiDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(YoutubeIntellijBuildingRestApiDemoApplication.class, args);
    }

}
