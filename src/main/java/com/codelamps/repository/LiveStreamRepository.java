package com.codelamps.repository;

import com.codelamps.exception.LiveStreamNotFoundException;
import com.codelamps.model.LiveStream;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class LiveStreamRepository {

    List<LiveStream> streams = new ArrayList<>();

    public LiveStreamRepository() {
        streams.add(new LiveStream("1",
                "Stream 1",
                "Description 1",
                "http://localhost:8080/streams/1",
                LocalDateTime.of(2022, 3, 15, 10, 0),
                LocalDateTime.of(2023, 3, 15, 10, 0)));
        streams.add(new LiveStream("2", "Stream 2", "Description 2", "http://localhost:8080/streams/2", null, null));
        streams.add(new LiveStream("3", "Stream 3", "Description 3", "http://localhost:8080/streams/3", null, null));
    }

    public List<LiveStream> findAll() {
        return streams;
    }

    public LiveStream findById(String id) throws LiveStreamNotFoundException {
        return streams.stream().filter(stream -> stream.id().equals(id)).findFirst().orElseThrow(LiveStreamNotFoundException::new);
    }

    public LiveStream create(LiveStream stream) {
        streams.add(stream);
        return stream;
    }

    public void update(LiveStream stream, String id) {
        LiveStream existing = streams.stream().filter(s -> s.id().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Stream not found"));
        int i = streams.indexOf(existing);
        streams.set(i, stream);
    }

    public void delete(String id) {
        streams.removeIf(stream -> stream.id().equals(id));
    }
}
