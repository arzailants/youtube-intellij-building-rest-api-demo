# AGENDA
 - Introduction
 - APIs
 - REST
 - Live Coding (Stand back!)
 - Resource
 - https://github.com/danvega/building-rest-apis-spring-boot

# About Dan Vega
 - Husband & Father
 - Software Developer for 20+ years
 - Spring Developer Advocate
 - Content Creator (www.danvega.dev)
   - Blogger
   - Youtube
   - Course Creator
 - Cleveland

# API

# REST

# DEPENDENCY
 - Spring Web
 - Validation
 - Lombok

# DEEPER DIVE
 - What is REST - https://restfulapi.net/
 - Spring HATEOAS - https://spring.io/projects/spring-hateoas
 - Spring Data REST - https://spring.io/projects/spring-data-rest
 - Documenting APIs with - Swagger / Spring REST Docs
 - Calling APIs using RESTTemplate or WebClient 
 

